import math

class Stack():
    def __init__(self):
        self.__data = list()

    def push(self, value):
        self.__data.append(value)

    def pop(self):
        return self.__data.pop()

    def size(self):
        return len(self.__data)

    def is_empty(self):
        return len(self.__data) == 0

    def get(self):
        return self.__data[::-1]

    def __repr__(self):
        reversed = self.__data[::-1]
        return ('\n'.join( str(val) for val in reversed))


#str1 = '( ( ) ) ( ( ))'
def brackets_checker( str ):
    if len(str) == 0:
        return False

    s = Stack()
    for item in str:
        if item == '(':
            s.push(item)
        elif item == ')':
            if s.is_empty():
                return False
            s.pop()
    return s.is_empty()

#result = brackets_checker(str)
#print(result)

def denary_to_binary( num ):
    stack = Stack()
    while num / 2 != 0:
        stack.push( num % 2 )
        num = math.floor(num / 2)

    return ''.join( str(val) for val in stack.get() )

res = denary_to_binary(196)
print(res)

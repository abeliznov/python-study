import random
# List()
# search(item)
# add(item)
# remove(item)
# size()
# append(item)
# insert(index, item)
# pop(index)

class Node():
    def __init__(self, item):
        self.__next = None
        self.__item = item

    def set_next(self, node):
        self.__next = node

    def get_next(self):
        return self.__next

    def get_item(self):
        return self.__item

    def set_item(self, item):
        self.__item = item


class LinkedList():
    def __init__(self):
        self.__head = None
        self.__tail = None

    def get_next(self):
        pass

    def set_next(self, node):
        pass

    def add(self, item):
        node = Node(item)
        if self.__head is None:
            self.__head = node
            self.__tail = node
        else:
            current = self.__head
            self.__head = node
            self.__head.set_next(current)

    def size(self):
        count = 0
        current = self.__head
        while current is not None:
            count += 1
            current = current.get_next()
        return count

    def search(self, item):
        current = self.__head
        if current == item:
            return True
        while current is not None:
            current = current.get_next()
            if current.get_item() == item:
                return True
        return False

    def remove(self, item):
        current = self.__head
        if current is None:
            return False
        if current.get_item() == item:
            self.__head = current.get_next()
            return True

        prev = None
        while current is not None:

            if current.get_item() == item:
                prev.set_next(current.get_next())
                return True

            prev = current
            current = current.get_next()

        return False

    def append(self, item):
        node = Node(item)
        current = self.__tail

        current.set_next(node)
        self.__tail = node

    def insert(self, index, item):
        new_node = Node(item)
        if self.__head is None or index == 0:
            self.add(new_node.get_item())
            return True

        current = self.__head
        count = 0
        inserted = False
        prev = None
        while current.get_next() is not None:

            if index == count:
                prev.set_next(new_node)
                new_node.set_next(current)
                inserted = True
                break

            prev = current
            current = current.get_next()
            count += 1

        if not inserted:
            current.set_next(new_node)

        return True

    def pop(self, index):
        if self.__head is None:
            raise ValueError("Список пуст")

        current = self.__head
        if index == 0:
            item = current.get_item()
            self.__head = current.get_next()
            return item

        count = 0
        prev = None
        while current.get_next() is not None:

            if index == count:
                item = current.get_item()
                prev.set_next(current.get_next())
                return item

            prev = current
            current = current.get_next()
            count += 1

        raise ValueError('Элемента с таким индексом нет!')

    def bubbleSort(self):

        sorted = False
        current = self.__head

        while not sorted:
            sorted = True
            prev = None
            while current is not None:
                if prev is not None:
                    if prev.get_item() > current.get_item():
                        sorted = False

                        self.add(current.get_item())
                        prev.set_next(current.get_next())
                        current.set_next(prev)

                prev = current
                current = current.get_next()

            current = self.__head







    def __repr__(self):
        if self.__head is None:
            return '[]'

        s = '['
        current = self.__head
        while current is not None:
            s += str(current.get_item()) + ','
            current = current.get_next()

        s = s[:-1]
        s += ']'
        return s


l = LinkedList()
l.add(15)
l.add(30)
l.add(45)
l.add(75)
l.remove(30)

l.insert(3, 22)
l.insert(45, 22)

i = 1
while i <= 20:
    l.insert(random.randrange(0, l.size() - 1), random.randrange(1, 999))
    i = i + 1


l.bubbleSort()

print(l)
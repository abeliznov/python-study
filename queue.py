import re

class Queue:
    def __init__(self):
        self.__data = list()

    def __repr__(self):
        return '\n'.join(str(item) for item in self.__data)

    def enqueue(self, val):
        self.__data.append(val)

    def dequeue(self):
        if(len(self.__data) > 0):
            return self.__data.pop(0)
        return None

    def back(self):
        if(len(self.__data) > 0):
            return self.__data[:-1]
        return None

    def front(self):
        if(len(self.__data) > 0):
            return self.__data[0]
        return None

    def size(self):
        return  len(self.__data)

    def clear(self):
        self.__data = list()

    def is_empty(self):
        return self.size() == 0

def hot_potato(names, num):
    queue = Queue()
    for name in names:
        queue.enqueue(name)

    eliminated = ''
    while queue.size() > 1:
        for i in range(num):
            queue.enqueue(queue.dequeue())
        eliminated = queue.dequeue()
        print(f'{eliminated} вышел из игры')

    return queue.dequeue()
names = ['Лёша', 'Вася', 'Катя', 'Петя', 'Саша']
#winner = hot_potato(names, 8)
#print(winner)
names = ['5 Лёша', '9 Вася', '3 Катя', '11 Петя', '5 Саша']
def queue_sort(lst):
    queue = Queue()
    for item in lst:
        if queue.is_empty():
            queue.enqueue(item)
        else:
            if queue.front()[0] > item[0]:
                prevItem = queue.dequeue()
                queue.enqueue(item)
                queue.enqueue(prevItem)
            else:
                queue.enqueue(item)
#queue_sort(names)

class DanceFloor:
    def __init__(self, fname):
        self.boys = Queue()
        self.girls = Queue()
        f = open(fname, encoding='utf-8', mode='r')
        for line in f:
            if f == '':
                break
            if line[0] == 'Д':
                self.girls.enqueue(line[2:].strip())
            else:
                self.boys.enqueue(line[2:].strip())
        f.close()

    def dance(self):
        min_counter = self.girls.size()
        max_counter = self.boys.size()
        if self.boys.size() < min_counter:
            min_counter = self.boys.size()
            max_counter = self.girls.size()
        i = 1

        while i <= min_counter:
            if self.boys.size() > 0 and self.girls.size() > 0:
                boy = self.boys.dequeue()
                girl = self.girls.dequeue()
                print(f"{boy} танцует с {girl}")
                self.boys.enqueue(boy)
                self.girls.enqueue(girl)

            i = i + 1

        if max_counter > min_counter:
            print(f"В очереди осталось {max_counter - min_counter} человек, первый в очереди {self.boys.front() if self.boys.size() > self.girls.size() else self.girls.front() }")


    def __repr__(self):
        print(self.boys)
        print(self.girls)

party = DanceFloor('queue.txt')
party.dance()
party.dance()










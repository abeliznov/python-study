import abc
class Stack:
    def __init__(self):
        self.__data = list()
    def push(self, value):
        self.__data.append(value)
    def pop(self):
        return self.__data.pop()
    def size(self):
        return len(self.__data)
    def is_empty(self):
        return len(self.__data) == 0
    def get(self):
        return self.__data[::-1]
    def __repr__(self):
        reversed = self.__data[::-1]
        return ('\n'.join( str(val) for val in reversed))

class InterpreterAbstract(abc.ABC):
    def __init__(self, code):
        self.code = code

    def execute(self):
        return self._parse()

    @abc.abstractmethod
    def _parse(self):
        pass

    @abc.abstractmethod
    def _evaluate(self, operand1, operator, operand2):
        pass

class Interpreter(InterpreterAbstract):
    def __init__(self, code):
        super().__init__(code)
        self.code = code.replace(' ', '')
        self.errors = list()
        self.operators = Stack()
        self.digits = Stack()
        self.floats = Stack()
        self.available_digits = [1,2,3,4,5,6,7,8,9,0]

    def execute(self):
        return self._parse()

    def _parse(self):
        self._check_brackets()
        if len(self.errors) > 0:
            return '\n'.join(self.errors)

        if self.code[0] == '(' and self.code[-1] == ')':
            self.code = self.code[1:-1]
        j = 0
        float_skip_counter = 0
        for i in self.code:
            if float_skip_counter > 0:
                float_skip_counter = float_skip_counter - 1
                j = j + 1
                continue

            if i != ')' and i != '(':
                if i.isdigit():
                    self.digits.push(int(i))
                elif i == '.' or i == ',':
                    float_before = []
                    float_after = []
                    k = j - 1
                    while self.code[k].isdigit():
                        if self.digits.is_empty():
                            break
                        float_before.append(self.code[k])
                        self.digits.pop()
                        k = k - 1

                    k = j + 1
                    while self.code[k].isdigit():
                        float_after.append(self.code[k])
                        k = k + 1

                    self.digits.push(float(''.join(float_before) + '.' + ''.join(float_after)))
                    float_skip_counter = k - j - 1
                else:
                    self.operators.push(i)
            elif i == ')':
                pass
                operator = self.operators.pop()
                operand2 = self.digits.pop()
                operand1 = self.digits.pop()
                operation_res = self._evaluate(operand1, operator, operand2)
                if operation_res is not False:
                    self.digits.push(operation_res)
                else:
                    break
            j = j + 1


        if len(self.errors) == 0:
            res = 0
            while not self.digits.is_empty():
                res = res + self.digits.pop()
            return res
        else:
            return '\n'.join(self.errors)

    def _check_brackets(self):
        s = Stack()
        if self.code[0] == ')':
            self.errors.append('Ошибка в скобках')
            return False
        for i in self.code:
            if i != ')' and i != '(':
                continue
            if s.is_empty() and i == ')':
                self.errors.append('Ошибка в скобках')
                return False
            if i == '(':
                s.push(i)
            else:
                s.pop()

        if s.is_empty():
            return True
        else:
            self.errors.append('Ошибка в скобках')
            return False

    def _evaluate(self, operand1, operator, operand2):
        if operator == '+':
            return operand1 + operand2
        elif operator == '-':
            return operand1 - operand2
        elif operator == '*':
            return operand1 * operand2
        elif operator == '/':
            return operand1 / operand2
        elif operator == '^':
            return operand1 ** operand2
        else:
            self.errors.append('Неизвестный оператор')
            return False

expression = ')1,5+((2.3 + 3)*(4^2))'
interpreter = Interpreter(expression)
res = interpreter.execute()
print(res)
